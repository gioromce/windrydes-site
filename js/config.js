window._config = {
    cognito: {
        userPoolId: 'us-east-2_i8fsT1wxC', // e.g. us-east-2_uXboG5pAb
        userPoolClientId: '3ro14tc92tfpss6k0iibum23bk', // e.g. 25ddkmj4v6hfsfvruhpfi7n4hv
        region: 'us-east-2' // e.g. us-east-2
    },
    api: {
        invokeUrl: 'https://5kxnng6jic.execute-api.us-east-2.amazonaws.com/prod' // e.g. https://rc7nyt4tql.execute-api.us-west-2.amazonaws.com/prod',
    }
};
